import React, { Component } from 'react';
import { TouchableOpacity, View, Text, TextInput, Image, LogBox, StyleSheet } from 'react-native';
 
export default class AboutScreen extends Component {
  
  render() {
    return (
        <View style={styles.container}>
          <View style={styles.header}></View>
          <Image style={styles.avatar} source={require('./images/me.jpg')}/>
          <View style={styles.body}>
            <View style={styles.bodyContent}>
              <Text style={styles.name}>Andi Ferry</Text>
              <Text style={styles.info}>UX Designer / Mobile developer</Text>
              <Text style={styles.description}>passsionate about coding</Text>
              <View style={{flex: 1, flexDirection: 'row', marginTop: 10, width: 600}}>
              <Text style={{fontSize: 20}}>Media Sosial</Text>
              </View>
              <View style={{flex: 1, flexDirection: 'row', marginTop: 10, width: 600}}>
        <View style={{width: 50, height: 50}}>
        <Image
        style={{width: 50, height: 50}}
        resizeMode ="contain"
        source = {require('./images/Twitter-Logo.png')}
        /> 
        </View>
        <View style={{width: 200, height: 50}}>
        <Text style={styles.info}>Twitter</Text> 
        </View>
      </View>
      <View style={{flex: 1, flexDirection: 'row', marginTop: 20, width: 600}}>
              <Text style={{fontSize: 20}}>Project</Text>
              </View>
              <View style={{flex: 1, flexDirection: 'row', marginTop: 10, width: 600}}>
        <View style={{width: 50, height: 50}}>
        <Image
        style={{width: 40, height: 40}}
        resizeMode ="contain"
        source = {require('./images/GitLab-Logo.png')}
        /> 
        </View>
        <View style={{width: 200, height: 50}}>
        <Text style={styles.info}>Gitlab</Text> 
        </View>
      </View>
            </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    header:{
      backgroundColor: "#00BFFF",
      height:200,
    },
    avatar: {
      width: 130,
      height: 130,
      borderRadius: 63,
      borderWidth: 4,
      borderColor: "white",
      marginBottom:10,
      alignSelf:'center',
      position: 'absolute',
      marginTop:130
    },
    name:{
      fontSize:22,
      color:"#FFFFFF",
      fontWeight:'600',
    },
    body:{
      marginTop:40,
    },
    bodyContent: {
      flex: 1,
      alignItems: 'center',
      padding:30,
    },
    name:{
      fontSize:28,
      color: "#696969",
      fontWeight: "600"
    },
    info:{
      fontSize:16,
      color: "#00BFFF",
      marginTop:10
    },
    description:{
      fontSize:16,
      color: "#696969",
      marginTop:10,
      textAlign: 'center'
    },
    buttonContainer: {
      marginTop:10,
      height:45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:250,
      borderRadius:30,
      backgroundColor: "#00BFFF",
    },
  });

/*
      <View style={{width: "100%", height: "100%"}}>
        
        <View style={{marginTop:70, paddingBottom:20, alignSelf:"center",
      alignContent:"center", alignItems:"center"}}>
        <Image
        style={{width: 100, height: 100}}
        resizeMode ="contain"
        source = {require('./images/logo.png')}
        />
      </View>

       <View>
      <Text style={{marginLeft: 10,color : "black", fontSize: 30, alignSelf:"center",
      alignContent:"center", alignItems:"center"}}> Andi Ferry </Text>
      </View> 

      <View style={{paddingTop: 10}}>
      <Text style={{marginLeft: 10,color : "black", alignSelf:"center",
      alignContent:"center", alignItems:"center", fontSize: 10}}> passionate about coding </Text>
      </View>
      <View style={{paddingTop: 30, marginLeft:50}}>
      <Text style={{fontSize: 15}}> Media Sosial </Text>
      <Image
        style={{width: 30, height: 30, marginTop:10}}
        resizeMode ="contain"
        source = {require('./images/Twitter-Logo.png')}
        /> <Text style={{}}> Twitter </Text>
      </View>

      </View>
      */