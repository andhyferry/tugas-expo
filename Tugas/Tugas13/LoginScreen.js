import React, { Component } from 'react';
import { TouchableOpacity, View, Text, TextInput, Image } from 'react-native';
 
export default class LoginScreen extends Component {
    constructor(props) {
        super(props)
        this.state= {
            username : "",
            password : ""
        }
    }

    validate_field=()=> {
        const {username, password} = this.state
        if (username == "") {
            alert("Username jangan dikosongi")
            return false
        } else if (password == "") {
            alert("Password jangan dikosongi")
            return false
        }
        return true
    }

    making_api_call=()=> {
      if(this.validate_field()){
          alert("Berhasil Login")
      }
    }

  render() {
    return (
      <View style={{width: "100%", height: "100%", justifyContent :"center", alignSelf:"center",
      alignContent:"center", alignItems:"center"}}>
        <View style={{paddingBottom:50}}>
        <Image
        style={{width: 100, height: 100}}
        resizeMode ="contain"
        source = {require('./images/logo.png')}
        />

        <Text style={{marginLeft: 10, paddingTop: 20,color : "black"}}> SanberApp </Text>
      </View>
      <View style={{paddingBottom:30}}>
        <TextInput placeholder={"Username"}
        onChangeText={(value)=> this.setState({username : value})}
        style = {{height : 42, width : 250, borderBottomWidth : 1}}
        />
      </View>
      <View>
        <TextInput placeholder={"Password"}
        onChangeText={(value)=> this.setState({password : value})}
        style = {{height : 42, width : 250, borderBottomWidth : 1}}
        />  
        </View>

      <View style = {{marginTop : 50, width : "80%"}}>
      <TouchableOpacity style = {{height : 42, width : "80%",
      justifyContent : "center", alignItems : "center", borderRadius : 40,
      backgroundColor : "blue", alignSelf: "center", textAlign : "center"}} 
      
      onPress={()=>this.making_api_call()}
      >
        
      <Text style={{color : "white"}}> LOGIN </Text>
    </TouchableOpacity>
      </View>

      <View style = {{marginTop : 30, width : "80%"}}>

      <Text style={{justifyContent : "center", alignItems : "center", borderRadius : 40,
      alignSelf: "center", textAlign : "center",paddingBottom:20, color : "black"}}> Belum punya akun? silahkan daftar </Text>

      </View>

      <View style = {{width : "80%"}}>
      <TouchableOpacity style = {{height : 42, width : "80%",
      justifyContent : "center", alignItems : "center", borderRadius : 40,
      backgroundColor : "blue", alignSelf: "center", textAlign : "center"}} >

      <Text style={{color : "white"}}> DAFTAR </Text>
    </TouchableOpacity>
      </View>

      </View>
    );
  }
}